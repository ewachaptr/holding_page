WHAT TO GET BEFORE YOU START:
- logo (svg)
- favicon.ico
- font (woff, woff2)
- colours (#RGB)
- text content (title, sub-title, contact info)

UPLOAD IMAGES TO images/ DIR

UPLOAD FONTS TO fonts/ DIR

EDIT index.html

EDIT _typography.scss

EDIT _variables.scss
